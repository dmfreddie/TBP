using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace FreddieBabord
{
    [BurstCompile]
    public struct SetMatrixFromFloat4x4Job : IJobParallelFor
    {
        public NativeArray<Matrix4x4> matrix4x4;
        [ReadOnly] public NativeArray<LocalToWorld> localToWorld;
        [ReadOnly] public float3 scaleOffset;
            
        public void Execute(int index)
        {
            matrix4x4[index] = localToWorld[index].Value + 
                               Matrix4x4.Scale(scaleOffset);
        }
    }
}