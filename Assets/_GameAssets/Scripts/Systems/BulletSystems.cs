using System;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Rendering;
using Unity.Transforms;

namespace FreddieBabord
{
    [UpdateInGroup(typeof(GameplaySystemGroup))]
    public class BulletSystemsGroup : ComponentSystemGroup
    {
    }

    [UpdateInGroup(typeof(BulletSystemsGroup))]
    public partial class BulletSpawnSystem : SystemBase
    {
        private EntityArchetype _bulletArchetype;
        private float _bulletFireTimer;
        private EntityQuery _playerQuery;
        private RenderMeshDescription? _renderMeshDescription;
        public RenderMesh? renderMesh;
        private BeginSimulationEntityCommandBufferSystem _ecbSystem;
        
        private const string BulletEntityName = "Bullet";
        
        protected override void OnCreate()
        {
            _bulletArchetype = EntityManager.CreateArchetype(typeof(Translation),
                typeof(LocalToWorld),
                typeof(Scale),
                typeof(Rotation),
                typeof(LifeTimeData),
                typeof(CurrentLifeTimeData),
                typeof(RenderMesh),
                typeof(RenderBounds),
                typeof(BulletTag));
            
            _playerQuery = EntityManager.CreateEntityQuery(typeof(PlayerTag),
                typeof(Rotation),
                typeof(Translation),
                typeof(LocalToWorld));
            
            _ecbSystem = World.GetExistingSystem<BeginSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            renderMesh ??= GameManager.Instance.BulletRenderMesh;
            _renderMeshDescription ??= new RenderMeshDescription(
                renderMesh.Value.mesh,
                renderMesh.Value.material,
                renderMesh.Value.castShadows,
                renderMesh.Value.receiveShadows);
            
            if (!_renderMeshDescription.HasValue)
            {
                return;
            }
            
            var playerEntity = _playerQuery.GetSingletonEntity();
            var playerRotation = EntityManager.GetComponentData<Rotation>(playerEntity);
            var playerTranslation = EntityManager.GetComponentData<Translation>(playerEntity);
            var playerLocalToWorld = EntityManager.GetComponentData<LocalToWorld>(playerEntity);
            var bulletLifeTime = GameManager.Instance.BulletLifetime;
            
            EntityCommandBuffer ecbFromSystem = _ecbSystem.CreateCommandBuffer();
            var ecb = ecbFromSystem.AsParallelWriter();
            
            Entities.ForEach((int entityInQueryIndex, in Entity ent, in BulletSpawnRequestTag _) =>
            {
                var idx = entityInQueryIndex;
                var spawnedBulletEntity = ecb.CreateEntity(idx, _bulletArchetype);

                ecb.SetName(idx, spawnedBulletEntity, BulletEntityName);
                
                ecb.SetComponent(idx, spawnedBulletEntity, new LocalToWorld());
                ecb.AddComponent<BulletTag>(idx, spawnedBulletEntity);
                ecb.SetComponent(idx, spawnedBulletEntity, new Rotation
                {
                    Value = playerRotation.Value
                });
                ecb.SetComponent(idx, spawnedBulletEntity, new Scale()
                {
                    Value = 0.65f
                });
                ecb.SetComponent(idx, spawnedBulletEntity, new LifeTimeData()
                {
                    Value = bulletLifeTime
                });
                ecb.SetComponent(idx, spawnedBulletEntity, new CurrentLifeTimeData()
                {
                    Value = 0f
                });
                ecb.SetComponent(idx, spawnedBulletEntity, new Translation
                {
                    Value = playerTranslation.Value + (playerLocalToWorld.Forward * 0.5f)
                });
                ecb.SetSharedComponent(idx, spawnedBulletEntity, renderMesh.Value);
                ecb.SetComponent(idx, spawnedBulletEntity, new RenderBounds
                {
                    Value = new AABB()
                    {
                        Center = renderMesh.Value.mesh.bounds.center,
                        Extents = renderMesh.Value.mesh.bounds.extents
                    }
                });
                RenderMeshUtility.AddComponents(idx, spawnedBulletEntity, ecb, _renderMeshDescription.Value);
                
                ecb.DestroyEntity(idx, ent);
                
            }).WithoutBurst().Run();
        }
    }
    
    [UpdateInGroup(typeof(BulletSystemsGroup))]
    public partial class BulletMovementSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var bulletSpeed = GameManager.Instance.BulletSpeed;
            var deltaTime = Time.DeltaTime;
            Entities.ForEach((ref Translation translation, in LocalToWorld localToWorld, in BulletTag _) =>
            {
                translation.Value += localToWorld.Forward * bulletSpeed * deltaTime;
            }).ScheduleParallel();
        }
    }

    [UpdateInGroup(typeof(BulletSystemsGroup))]
    public partial class BulletEnemyInteractionSystem : SystemBase
    {
        private EntityQuery _bulletQuery;
        private BeginSimulationEntityCommandBufferSystem _ecbSystem;
        private BuildPhysicsWorld _physicsWorldSystem;

        protected override void OnCreate()
        {
            _bulletQuery = EntityManager.CreateEntityQuery(typeof(BulletTag),
                typeof(Translation),
                typeof(LocalToWorld));

            _ecbSystem = World.GetExistingSystem<BeginSimulationEntityCommandBufferSystem>();
            _physicsWorldSystem = World.GetExistingSystem<BuildPhysicsWorld>();
        }

        protected override void OnUpdate()
        {
            var bulletTranslations = _bulletQuery.ToComponentDataArray<Translation>(Allocator.TempJob);
            var bulletL2ws = _bulletQuery.ToComponentDataArray<LocalToWorld>(Allocator.TempJob);
            var bulletEntities = _bulletQuery.ToEntityArray(Allocator.TempJob);
            var didHits = new NativeArray<int>(bulletTranslations.Length, Allocator.TempJob);

            var ecb = _ecbSystem.CreateCommandBuffer();
            
            var collisionWorld = _physicsWorldSystem.PhysicsWorld.CollisionWorld;
            NativeArray<RaycastInput> inputs = new NativeArray<RaycastInput>(bulletEntities.Length, Allocator.TempJob);
            NativeArray<RaycastHit> hits = new NativeArray<RaycastHit>(bulletEntities.Length, Allocator.TempJob);

            var setupHandle = new SetupRaycastJob()
            {
                Inputs = inputs,
                BulletTranslations = bulletTranslations,
                BulletL2W = bulletL2ws
            }.Schedule(inputs.Length, 4);
            
            var raycastHandle = RaycastJob.ScheduleBatchRayCast(collisionWorld, inputs, hits, didHits, setupHandle);

            var response = new RaycastResponseJob()
            {
                ecb = ecb.AsParallelWriter(),
                didHits = didHits,
                BulletEntities = bulletEntities,
                results = hits
            }.Schedule(inputs.Length, 4, raycastHandle);

            var d = inputs.Dispose(response);
            var d0 = hits.Dispose(d);
            var d1 = bulletL2ws.Dispose(d0);
            var d2 = didHits.Dispose(d1);
            var d3 = bulletEntities.Dispose(d2);
            var d4 = bulletTranslations.Dispose(d3);
            _ecbSystem.AddJobHandleForProducer(d4);
        }

        [BurstCompile]
        public struct SetupRaycastJob : IJobParallelFor
        {
            public NativeArray<RaycastInput> Inputs;
            [ReadOnly] public NativeArray<Translation> BulletTranslations;
            [ReadOnly] public NativeArray<LocalToWorld> BulletL2W;
            
            public void Execute(int index)
            {
                Inputs[index] = new RaycastInput()
                {
                    Start = BulletTranslations[index].Value,
                    End = BulletTranslations[index].Value + BulletL2W[index].Forward * 0.2f,
                    Filter = new CollisionFilter()
                    {
                        BelongsTo = ~0u,
                        CollidesWith = 1u << 1,
                        GroupIndex = 0
                    }
                };
            }
        }

        [BurstCompile]
        public struct RaycastResponseJob : IJobParallelFor
        {
            public EntityCommandBuffer.ParallelWriter ecb;
            [ReadOnly] public NativeArray<RaycastHit> results;
            [ReadOnly] public NativeArray<Entity> BulletEntities;
            [ReadOnly] public NativeArray<int> didHits;
            
            public void Execute(int index)
            {
                if (didHits[index] == 1)
                {
                    var hitEntity = results[index].Entity;
                    ecb.DestroyEntity(index, hitEntity);
                    ecb.DestroyEntity(index, BulletEntities[index]);
                    var addScoreEntity = ecb.CreateEntity(index);
                    ecb.AddComponent<GameManagerScoreData>(index, addScoreEntity);
                    ecb.SetComponent(index, addScoreEntity, new GameManagerScoreData
                    {
                        Value = 1
                    });
                }
            }
        }

        [BurstCompile]
        public struct RaycastJob : IJobParallelFor
        {
            [ReadOnly] public CollisionWorld world;
            [ReadOnly] public NativeArray<RaycastInput> inputs;
            public NativeArray<RaycastHit> results;
            public NativeArray<int> didHits;
            
            public unsafe void Execute(int index)
            {
                didHits[index] = world.CastRay(inputs[index], out var hit) ? 1 : 0;
                results[index] = hit;
            }
            
            public static JobHandle ScheduleBatchRayCast(
                CollisionWorld world,
                NativeArray<RaycastInput> inputs, 
                NativeArray<RaycastHit> results,
                NativeArray<int> didHits,
                JobHandle dependancy)
            {
                JobHandle rcj = new RaycastJob
                {
                    inputs = inputs,
                    results = results,
                    world = world,
                    didHits = didHits
                }.Schedule(inputs.Length, 4, dependancy);
                return rcj;
            }
        }
    }

}