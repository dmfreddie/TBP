using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using Collider = Unity.Physics.Collider;

namespace FreddieBabord
{
    [UpdateInGroup(typeof(GameplaySystemGroup))]
    public class EnemySystemsGroup : ComponentSystemGroup
    {
    }

    [BurstCompile,
     UpdateInGroup(typeof(EnemySystemsGroup))]
    public partial class EnemySpawnRequestHandlerSystem : SystemBase
    {
        private EntityArchetype _enemyEntity;
        private const string EnemyName = "Enemy";
        public BlobAssetReference<Collider> collider;
        private RenderMeshDescription? _renderMeshDescription;
        private RenderMesh? renderMesh;
        private BeginSimulationEntityCommandBufferSystem _ecbSystem;
        private EntityQuery _spawnRequestQuery;
        
        protected override void OnCreate()
        {
            _enemyEntity = EntityManager.CreateArchetype(
                typeof(Translation), 
                typeof(Rotation), 
                typeof(EnemyTag),
                typeof(LocalToWorld),
                typeof(GroundedData),
                typeof(RenderMesh),
                typeof(RenderBounds),
                typeof(PhysicsWorldIndex),
                typeof(PhysicsCollider));

            _spawnRequestQuery = GetEntityQuery(
                typeof(EnemySpawnRequestData));
            
            _ecbSystem = World.GetExistingSystem<BeginSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            renderMesh ??= GameManager.Instance.EnemyRenderMesh;
            
            _renderMeshDescription ??= new RenderMeshDescription(
                renderMesh.Value.mesh,
                renderMesh.Value.material,
                renderMesh.Value.castShadows,
                renderMesh.Value.receiveShadows);

            if (!_renderMeshDescription.HasValue)
            {
                return;
            }

            EntityCommandBuffer ecbFromSystem = _ecbSystem.CreateCommandBuffer();

            var bounds = renderMesh.Value.mesh.bounds;
            var mesh = renderMesh.Value;
            var rmDescription = _renderMeshDescription.Value;
            var col = collider;
            var enemyArchetype = _enemyEntity;
            
            var ecb = ecbFromSystem.AsParallelWriter();
            Entities.ForEach((int entityInQueryIndex, ref EnemySpawnRequestData spawnData, in Entity ent) =>
            {
                int idx = entityInQueryIndex;
                var position = spawnData.SpawnPosition;
                var spawnedEntity = ecb.CreateEntity(idx, enemyArchetype);
                ecb.SetName(idx, spawnedEntity, EnemyName);
                ecb.SetComponent(idx, spawnedEntity, new Translation { Value = position });
                ecb.SetComponent(idx, spawnedEntity, new LocalToWorld());
                ecb.SetComponent(idx, spawnedEntity, new GroundedData());
                ecb.SetSharedComponent(idx, spawnedEntity, mesh);
                ecb.SetSharedComponent(idx, spawnedEntity, new PhysicsWorldIndex());
                ecb.SetComponent(idx, spawnedEntity, new RenderBounds
                {
                    Value = new AABB
                    {
                        Center = bounds.center,
                        Extents = bounds.extents
                    }
                });
                ecb.SetComponent(idx, spawnedEntity, new PhysicsCollider { Value = col });

                RenderMeshUtility.AddComponents(idx, spawnedEntity, ecb, rmDescription);
 
                ecb.DestroyEntity(idx, ent);

            }).WithoutBurst().Run();
        }
    }

    [UpdateInGroup(typeof(EnemySystemsGroup))]
    public partial class EnemySpawnSystem : SystemBase
    {
        private EntityArchetype _enemyEntity;
        private float _internalTimer = 0f;
        
        private EntityQuery _enemyQuery;
        private EntityQuery _playerQuery;
        private EntityQuery _spawnZonesQuery;
        private BeginSimulationEntityCommandBufferSystem _ecbSystem;
        
        protected override void OnCreate()
        {
            _enemyEntity = EntityManager.CreateArchetype(
                typeof(EnemySpawnRequestData),
                typeof(MovementData)
                );
            
            _enemyQuery = EntityManager.CreateEntityQuery(typeof(EnemyTag));
            _playerQuery = EntityManager.CreateEntityQuery(typeof(PlayerTag), typeof(Translation));
            _spawnZonesQuery = EntityManager.CreateEntityQuery(typeof(SpawnZoneData));
            _ecbSystem = World.GetExistingSystem<BeginSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            _internalTimer += Time.DeltaTime;

            if (_internalTimer < GameManager.Instance.EnemySpawnDelay)
            {
                return;
            }

            var spawnedAmount = _enemyQuery.CalculateEntityCount();
            if (spawnedAmount < GameManager.Instance.EnemyMaxCount && !_spawnZonesQuery.IsEmpty)
            {

                var playerEntity = _playerQuery.GetSingletonEntity();
                var playerTranslation = EntityManager.GetComponentData<Translation>(playerEntity);
                
                int amountToSpawn = math.min(GameManager.Instance.EnemyMaxCount - spawnedAmount, GameManager.Instance.EnemyMaxEnemyFrameSpawnCount);
                var ecb = _ecbSystem.CreateCommandBuffer();

                var spawnZones = _spawnZonesQuery.ToComponentDataArray<SpawnZoneData>(Allocator.TempJob);
                var frameCount = (uint)UnityEngine.Time.frameCount;
                var spawnJob = new SpawnJob
                {
                    spawnZoneDataArray = spawnZones,
                    ecb = ecb.AsParallelWriter(),
                    archtypeToSpawn = _enemyEntity,
                    frameCount = frameCount,
                    playerPosition = playerTranslation.Value,
                    playerProtectionRadius = GameManager.Instance.EnemyPlayerProtectionRadius
                }.Schedule(amountToSpawn, 1);
                var spawnDispose = spawnZones.Dispose(spawnJob);
                
                _ecbSystem.AddJobHandleForProducer(spawnDispose);
            }
        }

        [BurstCompile]
        public struct SpawnJob : IJobParallelFor
        {
            public EntityCommandBuffer.ParallelWriter ecb;
            [ReadOnly] public EntityArchetype archtypeToSpawn;
            [ReadOnly] public uint frameCount;
            [ReadOnly] public float3 playerPosition;
            [ReadOnly] public float playerProtectionRadius;
            
            [ReadOnly] public NativeArray<SpawnZoneData> spawnZoneDataArray;

            public void Execute(int index)
            {
                Unity.Mathematics.Random rnd = Unity.Mathematics.Random.CreateFromIndex(frameCount + (uint)index);
                var idx = rnd.NextInt(0, spawnZoneDataArray.Length);
                var bounds = spawnZoneDataArray[idx].Bounds;
                var requestedSpawn = rnd.NextFloat3(bounds.min, bounds.max);

                if (math.distance(requestedSpawn, playerPosition) > playerProtectionRadius)
                {
                    var spawnedEntity = ecb.CreateEntity(index, archtypeToSpawn);
                    ecb.SetComponent(index, spawnedEntity, new EnemySpawnRequestData()
                    {
                        SpawnPosition = requestedSpawn
                    });
                    ecb.SetComponent(index, spawnedEntity, new MovementData()
                    {
                        baseWalkSpeed = 1
                    });
                }
            }
        }
    }
    
    [UpdateInGroup(typeof(EnemySystemsGroup))]
    public partial class EnemyMoveToPlayerSystem : SystemBase
    {
        private EntityQuery _playerQuery;

        protected override void OnCreate()
        {
            _playerQuery = EntityManager.CreateEntityQuery(
                typeof(PlayerTag), 
                typeof(Translation));
        }

        protected override void OnUpdate()
        {
            var playerEntity = _playerQuery.GetSingletonEntity();
            var playerPosition = EntityManager.GetComponentData<Translation>(playerEntity).Value;
            
            var deltaTime = Time.DeltaTime;
            
            Entities.ForEach((ref Translation translation, in EnemyTag _) =>
            {
                if(math.distance(playerPosition, translation.Value) > 1.5f)
                {
                    var direction = math.normalize(playerPosition - translation.Value);
                    translation.Value += direction * deltaTime;
                }
            }).ScheduleParallel();
        }
    }
}