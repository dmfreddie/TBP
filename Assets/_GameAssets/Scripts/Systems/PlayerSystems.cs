using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Rendering;
using RaycastHit = UnityEngine.RaycastHit;

namespace FreddieBabord
{
    [UpdateInGroup(typeof(GameplaySystemGroup))]
    public class PlayerSystemsGroup : ComponentSystemGroup
    {
    }

    [ UpdateInGroup(typeof(PlayerSystemsGroup))]
    public partial class PlayerAbilitySystemGroup : ComponentSystemGroup
    {
    }
    
    [ UpdateInGroup(typeof(PlayerSystemsGroup))]
    public partial class PlayerMovementSystem : SystemBase
    {
        private EntityQuery _cameraQuery;

        protected override void OnCreate()
        {
            _cameraQuery = EntityManager.CreateEntityQuery(
                typeof(CameraTag), 
                typeof(LocalToWorld));
        }

        protected override void OnUpdate()
        {
            var horizontal = Input.GetAxis("Horizontal");
            var vertical = Input.GetAxis("Vertical");

            var cameraEntity = _cameraQuery.GetSingletonEntity();
            var cameraLocalToWorld = EntityManager.GetComponentData<LocalToWorld>(cameraEntity);

            var deltaTime = Time.DeltaTime;

            Entities.ForEach((ref Translation translation, ref Rotation rotation, ref MovementData movementData, 
                in LocalToWorld ltw, in PlayerTag _) =>
            {
                var screenUp = (cameraLocalToWorld.Forward + cameraLocalToWorld.Up) / 2f;
                var direction = screenUp * vertical + cameraLocalToWorld.Right * horizontal;

                direction.y = 0f;
                translation.Value += direction * movementData.baseWalkSpeed * deltaTime;
            }).Schedule();
        }
    }

    [ UpdateInGroup(typeof(PlayerSystemsGroup))]
    public partial class PlayerLookSystem : SystemBase
    {
        private Camera _camera;
        private EntityQuery _playerQuery;
        BuildPhysicsWorld _physicsWorldSystem;
        
        protected override void OnCreate()
        {
            _playerQuery = EntityManager.CreateEntityQuery(
                typeof(PlayerTag),
                typeof(Translation),
                typeof(Rotation));
            
            _physicsWorldSystem = World.GetExistingSystem<BuildPhysicsWorld>();
        }
        
        protected override void OnUpdate()
        {
            _camera ??= Camera.main;
            
            var mousePosition = Input.mousePosition;

            if (Input.GetMouseButton(0) || Input.GetMouseButton(1))
            {
                var playerEntity = _playerQuery.GetSingletonEntity();
                var playerPosition = EntityManager.GetComponentData<Translation>(playerEntity);
                var playerRotation = EntityManager.GetComponentData<Rotation>(playerEntity);

                var ray = _camera.ScreenPointToRay(mousePosition);
                
                RaycastInput input = new RaycastInput()
                {
                    Start = ray.origin,
                    End = ray.origin + ray.direction * 1000,
                    Filter = new CollisionFilter()
                    {
                        BelongsTo = ~0u,
                        CollidesWith = ~0u, // all 1s, so all layers, collide with everything
                        GroupIndex = 0
                    }
                };
                
                var collisionWorld = _physicsWorldSystem.PhysicsWorld.CollisionWorld;
                bool haveHit = collisionWorld.CastRay(input, out var hit);
                if (haveHit)
                {
                    // see hit.Position
                    // see hit.SurfaceNormal
                    /*Entity e = physicsWorldSystem.PhysicsWorld.Bodies[hit.RigidBodyIndex].Entity;
                    return e;*/
                    
                    var direction = math.normalize((float3)hit.Position - playerPosition.Value);
                    direction.y = 0;

                    if (math.length(direction) > 0f)
                    {
                        playerRotation.Value = quaternion.LookRotation(direction, new float3(0, 1f, 0));
                        EntityManager.SetComponentData(playerEntity, playerRotation);
                    }
                }
            }
        }
    }

    [ UpdateInGroup(typeof(PlayerSystemsGroup))]
    public partial class CameraToPlayerSystem : SystemBase
    {
        private EntityQuery _cameraQuery;
        private EntityQuery _playerQuery;
        private float _cameraDistance = 15f;

        protected override void OnCreate()
        {
            _cameraQuery = EntityManager.CreateEntityQuery(
                typeof(CameraTag), 
                typeof(Translation),
                typeof(LocalToWorld));

            _playerQuery = EntityManager.CreateEntityQuery(
                typeof(PlayerTag),
                typeof(Translation));
        }

        protected override void OnUpdate()
        {
            if (_cameraQuery.IsEmpty)
            {
                return;
            } 
            
            var cameraEntity = _cameraQuery.GetSingletonEntity();
            var cameraTranslation = EntityManager.GetComponentData<Translation>(cameraEntity);
            var cameraLtW = EntityManager.GetComponentData<LocalToWorld>(cameraEntity);
            
            var players = _playerQuery.ToEntityArray(Allocator.Temp);
            float3 centerPoint = float3.zero;
            foreach (var player in players)
            {
                var playerTranslation = EntityManager.GetComponentData<Translation>(player);
                centerPoint += playerTranslation.Value;
            }

            centerPoint /= players.Length;

            cameraTranslation.Value = math.lerp(
                cameraTranslation.Value, 
                centerPoint - cameraLtW.Forward * _cameraDistance, 
                GameManager.Instance.CameraSmoothSpeed * Time.DeltaTime);
            EntityManager.SetComponentData(cameraEntity, cameraTranslation);
        }
    }

    [ UpdateInGroup(typeof(PlayerSystemsGroup))]
    public partial class PlayerFireSystem : SystemBase
    {
        private float _bulletFireTimer;

        protected override void OnUpdate()
        {
            if (Input.GetButton("Fire1"))
            {
                _bulletFireTimer += Time.DeltaTime;
                if (_bulletFireTimer >= (1f / GameManager.Instance.BulletPlayerFireRate))
                {
                    var bulletRequestEntity = EntityManager.CreateEntity();
                    EntityManager.AddComponent<BulletSpawnRequestTag>(bulletRequestEntity);
                    _bulletFireTimer = 0f;
                }
            }
        }
    }

    [ UpdateInGroup(typeof(PlayerAbilitySystemGroup))]
    public partial class PlayerDashAbilitySystemGroup : ComponentSystemGroup
    {
    }
    
    [ UpdateInGroup(typeof(PlayerDashAbilitySystemGroup))]
    public partial class PlayerDashInitilisationSystem : SystemBase
    {
        private EntityQuery _playerQuery;
        
        protected override void OnCreate()
        {
            _playerQuery = EntityManager.CreateEntityQuery(typeof(PlayerTag),
                typeof(Translation),
                ComponentType.Exclude<PlayerDashComponent>());
            
        }

        protected override void OnUpdate()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                var playerEntity = _playerQuery.GetSingletonEntity();

                if (!EntityManager.HasComponent<PlayerDashCooldownData>(playerEntity))
                {
                    EntityManager.AddComponent<PlayerDashCooldownData>(playerEntity);
                    EntityManager.SetComponentData(playerEntity, new PlayerDashCooldownData(){ Value = 0f});
                }
                
                var cooldown = EntityManager.GetComponentData<PlayerDashCooldownData>(playerEntity);
                if (cooldown.Value > 0)
                {
                    return;
                }
                
                EntityManager.AddComponent<PlayerDashComponent>(playerEntity);
                EntityManager.SetComponentData(playerEntity, new PlayerDashComponent()
                {
                    Duration = GameManager.Instance.PlayerDashDuration,
                    Speed = GameManager.Instance.PlayerDashSpeed
                });

                cooldown.Value = GameManager.Instance.PlayerDashCooldown;
                EntityManager.SetComponentData(playerEntity, cooldown);
            }
        }
    }
    
    [ UpdateInGroup(typeof(PlayerDashAbilitySystemGroup))]
    public partial class PlayerDashSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var deltaTime = Time.DeltaTime;
            EntityCommandBuffer ecb = new EntityCommandBuffer(Allocator.TempJob);
            Entities.ForEach((ref PlayerDashComponent dash, ref Translation translation, in LocalToWorld ltw, in Entity ent) =>
            {
                dash.Duration -= deltaTime;
                translation.Value += ltw.Forward * dash.Speed * deltaTime;

                if (dash.Duration <= 0f)
                {
                    ecb.RemoveComponent<PlayerDashComponent>(ent);
                }
                
            }).WithStructuralChanges().Run();
            ecb.Playback(EntityManager);
            ecb.Dispose();
        }
    }
    
    [ UpdateInGroup(typeof(PlayerDashAbilitySystemGroup))]
    public partial class PlayerDashCooldownSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var deltaTime = Time.DeltaTime;
            Entities.ForEach((ref PlayerDashCooldownData cooldown) =>
            {
                if (cooldown.Value > 0)
                {
                    cooldown.Value -= deltaTime;
                    if (cooldown.Value < 0)
                    {
                        cooldown.Value = 0f;
                    }
                }
            }).Schedule();
        }
    }

    [ UpdateInGroup(typeof(PlayerAbilitySystemGroup))]
    public partial class PlayerEMPAbilitySystemGroup : ComponentSystemGroup
    {
    }

    [UpdateInGroup(typeof(PlayerEMPAbilitySystemGroup))]
    public partial class PlayerEMPActivationAbilitySystem : SystemBase
    {
        private float _fireTimer;
        private EntityArchetype _empArchetype;
        private EntityQuery _playerQuery;
        private const float EmpDuration = 0.5f;
        private const float EmpExpansionSpeed = 40f;

        public float FireTime => _fireTimer;
        public float FireDelay => GameManager.Instance.AbilityEMPDelay;
        
        protected override void OnCreate()
        {
            _empArchetype = EntityManager.CreateArchetype(
                typeof(PlayerAbilityTag),
                typeof(AbilityEMPDurationData),
                typeof(AbilityEMPExpansionSpeed),
                typeof(Scale),
                typeof(Translation),
                typeof(LocalToWorld));

            _playerQuery = EntityManager.CreateEntityQuery(typeof(PlayerTag),
                typeof(Translation));
        }

        protected override void OnUpdate()
        {
            if (_fireTimer > 0)
            {
                _fireTimer -= Time.DeltaTime;
            }
            
            var playerEntity = _playerQuery.GetSingletonEntity();
            var playerTranslation = EntityManager.GetComponentData<Translation>(playerEntity);
            
            if (Input.GetKeyDown(KeyCode.Q) && _fireTimer <= 0f)
            {
                _fireTimer = FireDelay;

                var empEntity = EntityManager.CreateEntity(_empArchetype);
                EntityManager.SetComponentData(empEntity, new AbilityEMPDurationData()
                {
                    Value = EmpDuration
                });
                EntityManager.SetComponentData(empEntity, new AbilityEMPExpansionSpeed()
                {
                    Value = EmpExpansionSpeed
                });
                EntityManager.SetComponentData(empEntity, new Translation()
                {
                    Value = playerTranslation.Value
                });
                EntityManager.SetComponentData(empEntity, new LocalToWorld());
            }
        }
    }
    
    [UpdateInGroup(typeof(PlayerEMPAbilitySystemGroup))]
    public partial class PlayerEMPAbilitySystem : SystemBase
    {
        private EntityQuery _enemyQuery;
        private EntityQuery _empQuery;
        private Camera _mainCamera;
        
        protected override void OnCreate()
        {
            _enemyQuery = EntityManager.CreateEntityQuery(typeof(EnemyTag),
                typeof(Translation));

            _empQuery = EntityManager.CreateEntityQuery(typeof(PlayerAbilityTag),
                typeof(AbilityEMPDurationData),
                typeof(AbilityEMPExpansionSpeed),
                typeof(Scale),
                typeof(Translation));
        }

        protected override void OnUpdate()
        {
            var deltaTime = Time.DeltaTime;
            
            Entities.ForEach((ref Scale scale, in AbilityEMPExpansionSpeed speed) =>
            {
                scale.Value += speed.Value * deltaTime;
            }).Run();

            Entities.ForEach((ref AbilityEMPDurationData duration, in Entity ent) =>
            {
                duration.Value -= deltaTime;
                if (duration.Value <= 0)
                {
                    EntityManager.AddComponent<MarkEntityForDestructionTag>(ent);
                }
            }).WithStructuralChanges().Run();

            var abilityEmpMesh = GameManager.Instance.AbilityEmpMesh;
            var abilityEmpMaterial = GameManager.Instance.AbilityEmpMaterial;
            _mainCamera ??= Camera.main;
            
            Entities.ForEach((in LocalToWorld ltw, in AbilityEMPDurationData _) => { 
                Graphics.DrawMesh(abilityEmpMesh, 
                    ltw.Value, 
                    abilityEmpMaterial, 
                    1, 
                    _mainCamera, 
                    0, 
                    null, 
                    ShadowCastingMode.Off, 
                    false);
            }).WithoutBurst().Run();

            if (!_empQuery.IsEmpty)
            {
                var enemyEntities = _enemyQuery.ToEntityArray(Allocator.TempJob);
                var enemyTranslations = _enemyQuery.ToComponentDataArray<Translation>(Allocator.TempJob);
                var ecb = new EntityCommandBuffer(Allocator.TempJob);

                var empEntity = _empQuery.GetSingletonEntity();
                var scale = EntityManager.GetComponentData<Scale>(empEntity);
                var translation = EntityManager.GetComponentData<Translation>(empEntity);
                
                new EnemyEMPRadiusJob()
                {
                    EnemyLocation = enemyTranslations,
                    EnemyEntities = enemyEntities,
                    Ecb = ecb.AsParallelWriter(),
                    EmpScale = scale.Value,
                    EmpLocation = translation.Value
                }.Schedule(enemyTranslations.Length, 100).Complete();
                
                ecb.Playback(EntityManager);
                ecb.Dispose();
                enemyTranslations.Dispose();
                enemyEntities.Dispose();
            }
        }

        [BurstCompile]
        public struct EnemyEMPRadiusJob : IJobParallelFor
        {
            public NativeArray<Translation> EnemyLocation;
            public NativeArray<Entity> EnemyEntities;
            public EntityCommandBuffer.ParallelWriter Ecb;
            public float EmpScale;
            public float3 EmpLocation;

            public void Execute(int i)
            {
                if (math.distance(EnemyLocation[i].Value, EmpLocation) <= EmpScale / 2f)
                {
                    Ecb.AddComponent<MarkEntityForDestructionTag>(i, EnemyEntities[i]);
                    var addScoreEntity = Ecb.CreateEntity(i);
                    Ecb.AddComponent<GameManagerScoreData>(i, addScoreEntity);
                    Ecb.SetComponent(i, addScoreEntity, new GameManagerScoreData
                    {
                        Value = 1
                    });
                }
            }
        }
    }
}