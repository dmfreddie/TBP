using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace FreddieBabord
{
    [UpdateBefore(typeof(TransformSystemGroup))]
    public class GameplaySystemGroup : ComponentSystemGroup
    {
    }
    

    [UpdateInGroup(typeof(GameplaySystemGroup))]
    public partial class LifeTimeSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var deltaTime = Time.DeltaTime;

            Entities.ForEach((ref CurrentLifeTimeData currentLifeTimeData, in LifeTimeData lifeTimeData,
                in Entity entity) =>
            {
                currentLifeTimeData.Value += deltaTime;
                if (currentLifeTimeData.Value >= lifeTimeData.Value)
                {
                    EntityManager.AddComponent<MarkEntityForDestructionTag>(entity);
                }
            }).WithStructuralChanges().Run();
        }
    }

    [UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class EntityDestructionSystem : SystemBase
    {
        private EndSimulationEntityCommandBufferSystem _ecbSystem;
        private EntityQuery _destructionQuery;
        
        protected override void OnCreate()
        {
            _ecbSystem = World.GetExistingSystem<EndSimulationEntityCommandBufferSystem>();
            _destructionQuery = GetEntityQuery(ComponentType.ReadOnly<MarkEntityForDestructionTag>());
        }

        protected override void OnUpdate()
        {
            var ecbFromWorld = _ecbSystem.CreateCommandBuffer();
            var ecb = ecbFromWorld.AsParallelWriter();

            var destructionJob = new DestructionJob()
            {
                Entities = _destructionQuery.ToEntityArray(Allocator.TempJob),
                ecb = ecb
            };

            var handle = destructionJob.Schedule(_destructionQuery.CalculateEntityCount(), 1);
            var clearHandle = destructionJob.Entities.Dispose(handle);
            Dependency = clearHandle;
            _ecbSystem.AddJobHandleForProducer(clearHandle);
        }

        [BurstCompile]
        public struct DestructionJob : IJobParallelFor
        {
            public NativeArray<Entity> Entities;
            public EntityCommandBuffer.ParallelWriter ecb;
            
            public void Execute(int index)
            {
                ecb.DestroyEntity(index, Entities[index]);
            }
        }
    }

    [UpdateInGroup(typeof(GameplaySystemGroup))]
    public partial class LockRotationSystem : SystemBase
    {
        private BeginSimulationEntityCommandBufferSystem _ecbSystem;
        
        protected override void OnCreate()
        {
            _ecbSystem = World.GetExistingSystem<BeginSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            var ecbFromSystem = _ecbSystem.CreateCommandBuffer();
            var ecb = ecbFromSystem.AsParallelWriter();
            
            Entities
                .WithNone<InitialLockRotationData>()
                .ForEach((
                    int entityInQueryIndex,
                    ref Rotation rotation, 
                    in LockRotationData data, 
                    in Entity ent) =>
            {
                
                
                if (data.X || data.Y || data.Z)
                {
                    var quaternionRotationValue = (Quaternion)rotation.Value;
                    var rotationEuler = (float3)quaternionRotationValue.eulerAngles;
                        
                    ecb.AddComponent<InitialLockRotationData>(entityInQueryIndex, ent);
                        
                    ecb.SetComponent(entityInQueryIndex, ent, new InitialLockRotationData()
                    {
                        X = rotationEuler.x,
                        Y = rotationEuler.y,
                        Z = rotationEuler.z
                    });
                }
            }).ScheduleParallel();

            Entities.ForEach((
                ref Rotation rotation,
                in LockRotationData data,
                in InitialLockRotationData initialData,
                in Entity ent) =>
            {
                var quaternionRotationValue = (Quaternion)rotation.Value;
                var rotationEuler = (float3)quaternionRotationValue.eulerAngles;

                if (data.X)
                {
                    rotationEuler.x = initialData.X;
                }
                if (data.Y)
                {
                    rotationEuler.y = initialData.Y;
                }
                if (data.Z)
                {
                    rotationEuler.z = initialData.Z;
                }
                rotation.Value = quaternion.Euler(rotationEuler);
            }).ScheduleParallel();
        }
    }

    [UpdateInGroup(typeof(GameplaySystemGroup))]
    public partial class GameManagerScoreSystem : SystemBase
    {
        private EndSimulationEntityCommandBufferSystem _ecbSystem;
        
        protected override void OnCreate()
        {
            _ecbSystem = World.GetExistingSystem<EndSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            var ecb = _ecbSystem.CreateCommandBuffer();
            Entities.ForEach((GameManagerScoreData scoreData, Entity ent) =>
            {
                GameManager.Instance.Score += scoreData.Value;
                ecb.DestroyEntity(ent);
            }).WithoutBurst().Run();
        }
    }

    /*public partial class GroundingSystem : SystemBase
    {
        private EntityQuery _entitiesToGround;
        private float _raycastDistance = 0.5f;
        
        protected override void OnCreate()
        {
            _entitiesToGround = GetEntityQuery( 
                typeof(Translation),
                typeof(GroundedData));
        }

        protected override void OnUpdate()
        {
            var entityTranslations = _entitiesToGround.ToComponentDataArray<Translation>(Allocator.TempJob);
            var entityGroundedData = _entitiesToGround.ToComponentDataArray<GroundedData>(Allocator.TempJob);
            var raycastHits = new NativeArray<RaycastHit>(entityTranslations.Length, Allocator.TempJob);
            var setupRaycastJob = new SetupRaycastJob
            {
                translations = entityTranslations,
                raycasts = new NativeArray<RaycastCommand>(entityTranslations.Length, Allocator.TempJob),
                raycastDistance = _raycastDistance
            };
            var setupRaycastJobHandle = setupRaycastJob.Schedule(entityTranslations.Length, 10);
            var raycastJob = RaycastCommand.ScheduleBatch(setupRaycastJob.raycasts, raycastHits, 10, setupRaycastJobHandle);
            var concludeRaycastJobHandle = new ConcludeRaycastJob
            {
                hits = raycastHits,
                groundedData = entityGroundedData,
                
            }.Schedule(raycastHits.Length, 10, raycastJob);
            var entityTranslationsDispose = entityTranslations.Dispose(concludeRaycastJobHandle);
            var entityGroundedDataDispose = entityGroundedData.Dispose(entityTranslationsDispose);
            var raycastHitsDispose = raycastHits.Dispose(entityGroundedDataDispose);
            setupRaycastJob.raycasts.Dispose(raycastHitsDispose);
        }

        [BurstCompile]
        public struct SetupRaycastJob : IJobParallelFor
        {
            public NativeArray<RaycastCommand> raycasts;
            [ReadOnly] public NativeArray<Translation> translations;
            [ReadOnly] public float raycastDistance;
            
            public void Execute(int idx)
            {
                raycasts[idx] = new RaycastCommand(translations[idx].Value, Vector3.down, raycastDistance);
            }
        }
        
        [BurstCompile]
        public struct ConcludeRaycastJob : IJobParallelFor
        {
            public NativeArray<RaycastHit> hits;
            public NativeArray<GroundedData> groundedData;

            public void Execute(int idx)
            {
                var groundedValue = groundedData[idx];
                groundedValue.Value = hits[idx].triangleIndex >= 0;
                groundedData[idx] = groundedValue;
            }
        }
    }*/
}