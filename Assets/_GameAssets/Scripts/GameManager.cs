using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Rendering;
using UnityEngine;
using UnityEngine.Serialization;
using Material = UnityEngine.Material;

namespace FreddieBabord
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }

        [Header("Camera Config")]
        [SerializeField, Range(0.01f, 1f)] 
        private float cameraSmoothSpeed;
        public float CameraSmoothSpeed => cameraSmoothSpeed;

        [Header("Bullet Config")]
        [SerializeField]
        private float bulletSpeed = 20;
        public float BulletSpeed => bulletSpeed;
        [SerializeField]
        private float bulletLifetime = 10;
        public float BulletLifetime => bulletLifetime;
        
        [SerializeField]
        private float bulletPlayerFireRate = 100;
        public float BulletPlayerFireRate => bulletPlayerFireRate;

        [SerializeField]
        private Vector3 bulletRenderingScale;
        public float3 BulletRenderingScale => bulletRenderingScale;
        
        [SerializeField]
        private RenderMesh bulletRenderMesh;
        public RenderMesh BulletRenderMesh => bulletRenderMesh;

        [Header("Enemy Config")]
        [SerializeField]
        private float enemySpawnDelay = 1f;
        public float EnemySpawnDelay => enemySpawnDelay;
        
        [SerializeField]
        private int enemyMaxCount = 10000;
        public int EnemyMaxCount => enemyMaxCount;
        
        [SerializeField]
        private int enemyMaxFrameSpawnCount = 50;
        public int EnemyMaxEnemyFrameSpawnCount => enemyMaxFrameSpawnCount;
        
        [SerializeField]
        private int enemyPlayerProtectionRadius = 2;
        public int EnemyPlayerProtectionRadius => enemyPlayerProtectionRadius;
        
        [SerializeField,
         FormerlySerializedAs("renderMesh")]
        private RenderMesh enemyRenderMesh;
        public RenderMesh EnemyRenderMesh => enemyRenderMesh;
        
        [Header("Player Config")]
        [SerializeField, FormerlySerializedAs("playerEMPDelay")]
        private float abilityEMPDelay = 1f;
        public float AbilityEMPDelay => abilityEMPDelay;
        
        [SerializeField]
        private Mesh abilityEmpMesh;
        public Mesh AbilityEmpMesh => abilityEmpMesh;

        [SerializeField]
        private Material abilityEmpMaterial;
        public Material AbilityEmpMaterial => abilityEmpMaterial;
        
        [SerializeField]
        private float playerDashSpeed = 1f;
        public float PlayerDashSpeed => playerDashSpeed;
        
        [SerializeField]
        private float playerDashDuration = 1f;
        public float PlayerDashDuration => playerDashDuration;
        
        [SerializeField]
        private float playerDashCooldown = 2.5f;
        public float PlayerDashCooldown => playerDashCooldown;
        
        // ----------- Gameplay
        [HideInInspector] public int Score;

        
        void Awake()
        {
            Instance = this;

            foreach (var world in World.All)
            {
                var esrs = world.GetExistingSystem<EnemySpawnRequestHandlerSystem>();
                if (esrs != null)
                {
                    esrs.collider = Unity.Physics.BoxCollider.Create(new BoxGeometry
                    {
                        Center = float3.zero,
                        BevelRadius = 0f,
                        Orientation = quaternion.identity,
                        Size = new float3(1, 1, 1)
                    });
                }
            }
           
        }

        private void OnDestroy()
        {
            Instance = null;
        }
    }
}