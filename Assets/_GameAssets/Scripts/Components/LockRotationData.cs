﻿using Unity.Entities;

namespace FreddieBabord
{
    [GenerateAuthoringComponent]
    public struct LockRotationData : IComponentData
    {
        public bool X, Y, Z;
    }

    public struct InitialLockRotationData : IComponentData
    {
        public float X, Y, Z;
    }
}