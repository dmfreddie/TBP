﻿using System;
using Unity.Entities;
using UnityEngine;

namespace FreddieBabord
{
    public struct SpawnZoneData : IComponentData
    {
        public Bounds Bounds;
    }
    
    [ExecuteInEditMode]
    public class SpawnZoneReference : MonoBehaviour, IConvertGameObjectToEntity
    {
        public Bounds bounds;
        
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponent<SpawnZoneData>(entity);
            var boundsStart = bounds;
            boundsStart.center = transform.position;
            dstManager.SetComponentData(entity, new SpawnZoneData()
            {
                Bounds = bounds
            });
            dstManager.SetName(entity, gameObject.name);
        }

        private void Update()
        {
            bounds.center = transform.position;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            
            Gizmos.DrawWireCube(bounds.center, bounds.size);
        }
    }
}