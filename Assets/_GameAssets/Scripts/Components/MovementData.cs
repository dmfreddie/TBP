using Unity.Entities;

namespace FreddieBabord
{

    [GenerateAuthoringComponent]
    public struct MovementData : IComponentData
    {
        public float baseWalkSpeed;
        public float baseDashSpeed;
        public float currentWalkSpeed;
        public float currentDashSpeed;
    }

}