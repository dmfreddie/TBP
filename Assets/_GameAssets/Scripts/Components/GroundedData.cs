﻿using Unity.Entities;

namespace FreddieBabord
{
    [GenerateAuthoringComponent]
    public struct GroundedData : IComponentData
    {
        public bool Value;
    }
}