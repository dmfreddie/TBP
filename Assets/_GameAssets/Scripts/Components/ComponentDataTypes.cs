using Unity.Entities;
using Unity.Mathematics;

namespace FreddieBabord
{

    public struct EnemySpawnRequestData : IComponentData
    {
        public float3 SpawnPosition;
    }

    public struct BulletSpawnRequestTag : IComponentData
    {
    }

    public struct MarkEntityForDestructionTag : IComponentData
    {
    }

    public struct LifeTimeData : IComponentData
    {
        public float Value;
    }

    public struct CurrentLifeTimeData : IComponentData
    {
        public float Value;
    }

    public struct PlayerAbilityTag : IComponentData
    {
    }

    public struct AbilityEMPDurationData : IComponentData
    {
        public float Value;
    }

    public struct AbilityEMPExpansionSpeed : IComponentData
    {
        public float Value;
    }

    public struct PlayerDashComponent : IComponentData
    {
        public float Duration;
        public float Speed;
    }

    public struct PlayerDashCooldownData : IComponentData
    {
        public float Value;
    }

    public struct GameManagerScoreData : IComponentData
    {
        public int Value;
    }
}