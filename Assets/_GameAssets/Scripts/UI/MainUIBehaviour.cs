﻿using System;
using System.Linq;
using Unity.Entities;
using UnityEngine;
using UnityEngine.UIElements;

namespace FreddieBabord.UI
{
    [RequireComponent(typeof(UIDocument))]
    public class MainUIBehaviour : MonoBehaviour
    {
        private UIDocument _uiDocument;
        private Label _scoreLabel;
        private Label _timeLabel;
        private VisualElement _ability1Element;
        private VisualElement _ability2Element;
        private VisualElement _mainAbilityElement;
        private EntityManager _defaultEntityManager;
        private World _defaultWorld;
        
        private PlayerEMPActivationAbilitySystem _activationSystem;
        private EntityQuery _playerQuery;
        
        void Start()
        {
            _uiDocument = GetComponent<UIDocument>();
            
            _scoreLabel = _uiDocument.rootVisualElement.Q<Label>("ScoreLabel");
            _timeLabel = _uiDocument.rootVisualElement.Q<Label>("TimeLabel");
            _ability1Element = _uiDocument.rootVisualElement.Q<VisualElement>("Ability1").Q<VisualElement>("AbilityFill");
            _ability2Element = _uiDocument.rootVisualElement.Q<VisualElement>("Ability2").Q<VisualElement>("AbilityFill");
            _mainAbilityElement = _uiDocument.rootVisualElement.Q<VisualElement>("AbilityHero").Q<VisualElement>("AbilityFill");
            
            _defaultWorld = World.All[0];
            _defaultEntityManager = _defaultWorld.EntityManager;
            _activationSystem = _defaultWorld.GetExistingSystem<PlayerEMPActivationAbilitySystem>();
            
            _playerQuery = _defaultEntityManager.CreateEntityQuery(typeof(PlayerTag),
                ComponentType.Exclude<PlayerDashComponent>());
        }

        void Update()
        {
            _scoreLabel.text = $"Score: {GameManager.Instance.Score}";

            var currentTime = Time.timeSinceLevelLoad;
            var ts = TimeSpan.FromSeconds(currentTime);
            _timeLabel.text = $"Time: {ts.Minutes:00}:{ts.Seconds:00}";

            
            
            // Ability 1 - TBD
            var activationPercentage = _activationSystem.FireTime / _activationSystem.FireDelay;
            //_ability1Element.style.height =  new StyleLength(new Length(activationPercentage * 100, LengthUnit.Percent));;
            
            // Ability 2 - Dash
            float dashValue = 0f;
            if (!_playerQuery.IsEmpty)
            {
                var playerEntity = _playerQuery.GetSingletonEntity();
                if (_defaultEntityManager.HasComponent<PlayerDashCooldownData>(playerEntity))
                {
                    dashValue = _defaultEntityManager.GetComponentData<PlayerDashCooldownData>(playerEntity).Value;
                }
            }

            activationPercentage = dashValue / GameManager.Instance.PlayerDashCooldown;
            _ability2Element.style.height = new StyleLength(new Length(activationPercentage * 100, LengthUnit.Percent));
            
            // Ability Hero - EMP
            activationPercentage = _activationSystem.FireTime / _activationSystem.FireDelay;
            _mainAbilityElement.style.height = new StyleLength(new Length(activationPercentage * 100, LengthUnit.Percent));
        }
    }
}